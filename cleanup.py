#! /usr/bin/env python3


from fnmatch import fnmatch
from os import path
import os
import boto3
import botocore

# Setup boto3 session & client
sess = boto3.Session().resource('ec2')
client = boto3.client('ec2', region_name='us-east-1')
ClientError = botocore.exceptions.ClientError

# find and terminate ec2 instance
try:
    ec2 = client.describe_instances(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    'fetch-ssh',
                ]
            },
            {
                'Name': 'instance-state-name',
                'Values': [
                    'running',
                ]
            },
        ],
    )
    print('Terminating Instance %s' % ec2["Reservations"][0]["Instances"][0]["InstanceId"])
    instance = sess.Instance(ec2["Reservations"][0]["Instances"][0]["InstanceId"])
    instance.terminate()
    instance.wait_until_terminated()

except ClientError as ce:
    print(ce)

# find and delete security group
try:
    sg = client.describe_security_groups(
            GroupNames=[
                'fetch-ssh',
            ],
        )
    print('Deleting Security Group %s' % sg["SecurityGroups"][0]["GroupId"])
    client.delete_security_group(GroupId=sg["SecurityGroups"][0]["GroupId"])

except ClientError as ce:
    print(ce)

# delete ssh keys from local folder
def globber(rootpath, wildcard):
    for root, dirs, files in os.walk(rootpath):
        for file in files:
            if fnmatch(file, wildcard):
                yield path.join(root, file)

for f in globber(os.getcwd(), "*.key"):
    os.remove(f)

