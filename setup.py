#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

import boto3
import botocore
import yaml
import os

from dateutil import parser

# Setup boto3 session & client
sess = boto3.Session().resource('ec2')
client = boto3.client('ec2', region_name='us-east-1')
ClientError = botocore.exceptions.ClientError

# Slurp in config.yaml
with open("config.yaml", 'r') as conf:
    try:
        y = yaml.load(conf.read(), Loader=yaml.FullLoader)
    except IOError:
        print("config.yaml not found.")
        sys.exit(1)

# Find latest Amazon Linux2 AMI

def newestImage(list):
    l = None
    for i in list:
        if not l:
            l = i
            continue
        if parser.parse(i['CreationDate']) > parser.parse(l['CreationDate']):
            l = i
    return l

try:
    amiFilters = [ {
            'Name': 'name',
            'Values': [y["server"]["ami_type"] + '-*']
        },{
            'Name': 'architecture',
            'Values': [y["server"]["architecture"]]
        },{
            'Name': 'owner-alias',
            'Values': ['amazon']
        },{
            'Name': 'owner-id',
            'Values': ['137112412989']
        },{
            'Name': 'state',
            'Values': ['available']
        },{
            'Name': 'root-device-type',
            'Values': [y["server"]["root_device_type"]]
        },{
            'Name': 'virtualization-type',
            'Values': [y["server"]["virtualization_type"]]
        },{
            'Name': 'hypervisor',
            'Values': ['xen']
        },{
            'Name': 'image-type',
            'Values': ['machine']
        } ]

    amiResp = client.describe_images(Owners=['amazon'], Filters=amiFilters)
    ami = newestImage(amiResp['Images'])
    ami = ami['ImageId']
except ClientError as ce:
    print(ce)

# Find VPC
try:
    vpcResp = client.describe_vpcs()
    vpcId = vpcResp.get('Vpcs', [{}])[0].get('VpcId', '')
except ClientError as ce:
    print(ce)

# Create SG
try:
    sgId = client.describe_security_groups(GroupNames=['fetch-ssh'])['SecurityGroups']
    sgId = sgId[0]['GroupId']
except ClientError:
    print("Security Group doesn't exist, creating it...")
    sgResp = client.create_security_group(GroupName='fetch-ssh', Description='Allow SSH Ingress', VpcId=vpcId)
    sgId = sgResp["GroupId"]
    rules = client.authorize_security_group_ingress(GroupId=sgId,
            IpPermissions=[
                {'IpProtocol': 'tcp',
                'FromPort': 22,
                'ToPort': 22,
                'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
    ])

# Generate SSH Keys for user0 and user1
users = y["server"]["users"]
prvKey = []
pubKey = []
for user in users:
    privateKey = rsa.generate_private_key(backend=default_backend(), public_exponent=65537, \
        key_size=2048)
    publicKey = privateKey.public_key().public_bytes(serialization.Encoding.OpenSSH, \
        serialization.PublicFormat.OpenSSH)
    pem = privateKey.private_bytes(encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption())
    prvKey.append(pem.decode('utf-8'))
    pubKey.append(publicKey.decode('utf-8'))

    with open('./%s-private.key' %user["login"], 'wb') as file:
        os.chmod('./%s-private.key' %user["login"], 0o600)
        file.write(pem)
    with open('./%s-pub.key' %user["login"], 'wb') as file:
        os.chmod('./%s-pub.key' %user["login"], 0o600)
        file.write(publicKey)

# Create EC2 Instance
try:
    # Setup user_data to format drive
    dir = FileSystemLoader('templates/')
    env = Environment(loader=dir)
    tmpl = env.get_template('cloud-config.tmpl')
    user0 = y["server"]["users"][0]["login"]
    user1 = y["server"]["users"][1]["login"]
    user0_sshkeys = pubKey[0]
    user1_sshkeys = pubKey[1]
    disk1_fs = y["server"]["volumes"][1]["type"]
    disk1_dev = y["server"]["volumes"][1]["device"]
    disk1_mount = y["server"]["volumes"][1]["mount"]
    userData = tmpl.render(user0=user0, user1=user1, user0_sshkeys=user0_sshkeys,
            user1_sshkeys=user1_sshkeys, disk1_fs=disk1_fs, disk1_dev=disk1_dev,
            disk1_mount=disk1_mount)

    instances = client.run_instances(
        BlockDeviceMappings=[
            {
                'DeviceName': y["server"]["volumes"][0]["device"],
                'Ebs': {

                    'DeleteOnTermination': True,
                    'VolumeSize': y["server"]["volumes"][0]["size_gb"],
                    'VolumeType': 'gp2',
                    'DeleteOnTermination':True
                },
            },
            {
                'DeviceName': y["server"]["volumes"][1]["device"],
                'Ebs': {

                    'DeleteOnTermination': True,
                    'VolumeSize': y["server"]["volumes"][1]["size_gb"],
                    'VolumeType': 'gp2',
                    'DeleteOnTermination':True
                },
            },
        ],
        ImageId=ami,
        KeyName='fetch-ssh',
        InstanceType=y["server"]["instance_type"],
        UserData=userData,
        MaxCount=y["server"]["max_count"],
        MinCount=y["server"]["min_count"],
        Monitoring={'Enabled': False},
        SecurityGroupIds=[sgId,],
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'fetch-ssh'
                    },
                ]
            },
        ],
    )

    # Wait on the instance to run so we can get the public IP and other info
    instance = instances["Instances"][0]
    instanceId = instance["InstanceId"]
    i = sess.Instance(instanceId)
    i.wait_until_running()
    i.load()

except ClientError as ce:
    print(ce)

# Print out next steps
print("")
print('EC2 instance %s is online' % instanceId)
print("")
print("use ssh to connect to the instance:")
print('ssh -i user1-private.key user1@%s' % i.public_ip_address)
print('ssh -i user2-private.key user2@%s' % i.public_ip_address)
