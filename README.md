# coding exercise
## requirements for challenge

1. run your program
2. deploy vertual machine
3. ssh into instance as user1 & user2
4. read & write to each of the two volumes

## prerequisites
#### aws
* you must have access to AWS with a valid IAM user/role with permissions to create an ec2 instance.
* you must have your `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` exported into your shell environment.  This is accomplished by [setting up the AWS cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) 
* a VPC with an Internet Gateway, Subnets, Routes created in `us-east-1` region

#### software
* awscli installed
* python3 installed
* boto3 installed

## warnings
this python script is intended for demonstration purposes only.  It is not production ready.  There are a number of security issues with this script that are set by default.  
* no hardened AMI Image
* no keypair created or managed by aws
* volumes are not encrypted
* security group created has open (`0.0.0.0/0`) to the world access on port `22` for ssh

## running the demo
please make sure you read the prerequisites above before continuing.

1. clone this repo to a directory on your pc

```sh
> git clone https://gitlab.com/nick.hudson/fetch-python.git
> cd fetch-python
```

2. execute the python script to build out your ec2 instance

```sh
> ./setup.py
```

this will create everything you need to ssh into the ec2 instance that is created.  There will be some output to the script that will have your next steps.  The output will be similar to this, your output will be specific for your instance and not what is below.

```sh
>  ./setup.py
Security Group doesn't exist, creating it...

EC2 instance i-0fef9890d161faa86 is online

use ssh to connect to the instance:
ssh -i user1-private.key user1@54.173.243.100
ssh -i user2-private.key user2@54.173.243.100
```

3. once the instance is up, you should be able to ssh into the instance using the provided output from the script. You should be able to write to both the `/` and `/data` partitions on the instance

```sh
> ssh -i user1-private.key user1@54.173.243.100
[user1@ip-172-31-47-4 ~]$ echo "hello user1" >$HOME/test.txt
[user1@ip-172-31-47-4 ~]$ echo "hello user1" >/data/test.txt
[user1@ip-172-31-47-4 ~]$ cat $HOME/test.txt
hello user1
[user1@ip-172-31-47-4 ~]$ cat /data/test.txt
hello user1
```

4. you can repeat set #3 for the `user2` user as well.  Both users have the same permissions.

```sh
> ssh -i user2-private.key user2@54.173.243.100
[user2@ip-172-31-47-4 ~]$ echo "hello user2" >$HOME/test.txt
[user2@ip-172-31-47-4 ~]$ echo "hello user2" >/data/test2.txt
[user2@ip-172-31-47-4 ~]$ cat $HOME/test.txt
hello user2
[user2@ip-172-31-47-4 ~]$ cat /data/test2.txt
hello user2
```

## Cleanup
i have provided you with a cleanup script to use if you wish.  This will terminate the EC2 instance and the security group that was created.

```sh
> ./cleanup.py
Terminating Instance i-0fef9890d161faa86
Deleting Security Group sg-0abaf5f427bfddd80
```